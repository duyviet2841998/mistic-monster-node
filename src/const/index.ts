import { Code } from '..'
import { Message } from '../enums'

const success_result = {
    code: Code.success,
    msg: Message.success,
}

export { success_result }
