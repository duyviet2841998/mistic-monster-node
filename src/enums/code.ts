enum Code {
    success = 1,
    error = 0,
    not_found = -1,
    request_err = -2,
    err_private_key = 2,
    unknown = 4,
}

export { Code }
