import { Code } from '..';
import { Message } from '../enums';
declare const success_result: {
    code: Code;
    msg: Message;
};
export { success_result };
