"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.success_result = void 0;
const __1 = require("..");
const enums_1 = require("../enums");
const success_result = {
    code: __1.Code.success,
    msg: enums_1.Message.success,
};
exports.success_result = success_result;
