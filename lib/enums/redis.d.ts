declare enum Store {
    blocks = "mmc_blocks",
    txs = "mmc_tsx",
    account = "mmc_acc",
    monster = "mmc_monster",
    wallet = "mmc_wallet",
    node = "mmc_node",
    privacy = "mmc_privacy"
}
export { Store };
