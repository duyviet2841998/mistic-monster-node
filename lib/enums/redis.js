"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Store = void 0;
var Store;
(function (Store) {
    Store["blocks"] = "mmc_blocks";
    Store["txs"] = "mmc_tsx";
    Store["account"] = "mmc_acc";
    Store["monster"] = "mmc_monster";
    Store["wallet"] = "mmc_wallet";
    Store["node"] = "mmc_node";
    Store["privacy"] = "mmc_privacy";
})(Store || (Store = {}));
exports.Store = Store;
